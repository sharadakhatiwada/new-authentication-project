<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::resource('clients','ClientController');
Route::resource('credits','CreditController');

Route::get('/credits/viewCredit/{client_id}', 'CreditController@viewCredit')->name('viewCredit');
Route::get('/credits/addCredit/{client_id}', 'CreditController@addCredit')->name('addCredit');
Route::post('/credits/store/{client_id}', 'CreditController@store')->name('credits.store');
Route::get('/credits/edit/{id}/{client_id}', 'CreditController@edit')->name('credits.edit');
Route::put('/credits/update/{id}/{client_id}', 'CreditController@update')->name('credits.update');
Route::delete('/credits/destroy/{id}/{client_id}', 'CreditController@destroy')->name('credits.destroy');
Route::get('/clients/printAllCredit/{client_id}','ClientController@printAllCredit')->name('clients.printAllCredit');
// Route::get('printAllCredit', 'ClientController@printAllCredit')->name('clinets.printAllCredit');




// Route::get('/client/printAllCredit',[ 'as' => 'print.printAllCredit, 
// 'uses' => 'ClientController@printAllCredit']);
// <a href="{{route('client.printAllCredit')}}">Print PDF</a>

// Route::get('clients/printAllCredit/{credit->id}', function (Request $request, $credit) {
//     return $request->user()->downloadcredit($credit, [
//         'vendor' => 'Your Company',
//         'product' => 'Your Product',
//     ]);
// });






// Route::get('/viewCredit/{id}','CreditController@viewCredit')->name;

