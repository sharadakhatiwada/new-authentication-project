<?php

namespace App;


use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    //
    /**
     * The attributes that are mass assignable.
     *

     // migration ko field haru lyayera rakheko
     * @var array
     */
// credit ko filled client le access  gareko
    public function credits(){
         return $this->hasMany('App\Credit');
    }
    

    protected $fillable = [        
        'name',              
        'address',  
        'phonenumber',
        'balance'    
    ];
    

}
