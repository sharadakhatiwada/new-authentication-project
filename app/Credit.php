<?php

namespace App;
use App\Credit;

use Illuminate\Database\Eloquent\Model;

class Credit extends Model
{
    //



    // made model using command line php artisan make:modal ClientCredit 
      // migration ko field haru lyayera rakheko
      //parent class saga access gareko

      public function client()
      {
          return $this->belongsTo('App\Client');
      }

     
     /*
     *var array
     
     */
    

    protected $fillable = [        
        
        'transaction_date',
        'particular',  
        'debit',
        'credit',
        'client_id',
        'image_name',
        'image_url'

    ];
}
