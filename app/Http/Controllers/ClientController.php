<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use DB;
use PDF;
use App\Client;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
            $clients = Client::all();        
            return view('clients.index', compact('clients'));    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('clients.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([            
            'name'=>'required',                       
            'address'=>'required', 
            'phonenumber'=>'required'       
        ]);        
        $client = new Client([            
            'name' => $request->get('name'),                        
            'address' => $request->get('address'), 
            'phonenumber'=>$request->get('phonenumber')               
            ]);        
        $client->save(); 
        

        return redirect('/clients')->with('success', 'Client saved!');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $client = Client::find($id);        
        return view('clients.edit', compact('client'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
       $request->validate([            
            'name'=>'required',                       
            'address'=>'required', 
            'phonenumber'=>'required'       
        ]);        
        $client = Client::find($id);        
        $client->name =  $request->get('name');             
        $client->address = $request->get('address');  
        $client->phonenumber = $request->get('phonenumber');          
        $client->save();        
        return redirect('/clients')->with('success', 'Client updated!');
    }
    public function destroy($id)
    {
        //
        $client = Client::find($id);        
        $client->delete();        
        return redirect('/clients')->with('success', 'Client deleted!');
 
   }
//    public function printAllCredit($id){
//      $credit=$this->get_credits();
//    $domPdfPath = base_path( 'vendor/dompdf/dompdf');
// \PhpOffice\PhpWord\Settings::setPdfRendererPath($domPdfPath);
// \PhpOffice\PhpWord\Settings::setPdfRendererName('DomPDF');
   
//   return view('dynamic_pdf')->with('credits',$credit);

   

//    }
   public function printAllCredit($client_id)
    {
        // 
    
     $pdf = \App::make('dompdf.wrapper');
     $pdf->loadHTML($this->convert_client_data_to_html($client_id));
     return $pdf->stream();
    }

    function convert_client_data_to_html($client_id)
    {
        $client = Client::find($client_id); 

     $credits= $client->credits;
   

     $output = '
   
     <h3 align="center">Client Data</h3>
     <table width="100%" style="border-collapse: collapse; border: 0px;">
     
     <tr>
     <th style="border: 1px solid; padding:12px;" width="20%">Name</th>
     <th style="border: 1px solid; padding:12px;" width="30%">Address</th>
     <th style="border: 1px solid; padding:12px;" width="30%">Balance</th>
     </tr>
     
     
     <tr>
     <td style="border: 1px solid; padding:12px;">'.$client->name.'</td>
    <td style="border: 1px solid; padding:12px;">'.$client->address.'</td>
    <td style="border: 1px solid; padding:12px;">'.$client->balance.'</td>
      
     </tr>
    
     
     
    

     
     
     <table class="table table-striped">  
     <table width="100%" style="border-collapse: collapse; border: 0px;"> 
      
     
      <tr>
      <th style="border: 1px solid; padding:12px;" width="20%">Date</th>
    <th style="border: 1px solid; padding:12px;" width="30%">Particular</th>
    <th style="border: 1px solid; padding:12px;" width="20%">Credit</th>
    <th style="border: 1px solid; padding:12px;" width="30%">Debit</th>
   
    
   </tr>
     ';  
     foreach($credits as $credit)
     {
      $output .= '
      <tr>
      <td style="border: 1px solid; padding:12px;">'.$credit->date.'</td>
       <td style="border: 1px solid; padding:12px;">'.$credit->particular.'</td>
       <td style="border: 1px solid; padding:12px;">'.$credit->credit.'</td>
       <td style="border: 1px solid; padding:12px;">'.$credit->debit.'</td>
     
      </tr>
      ';
      
     }
     $output .= '</table>';
     return $output;

    }
    

    

  }