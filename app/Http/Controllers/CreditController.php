<?php

namespace App\Http\Controllers;
use File;
use App\Credit;
use App\Client;
use Illuminate\Http\Request;


class CreditController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
            $credits = Credit::all();        
            return view('credits.index', compact('credits'));    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('credits.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $client_id)
    {
        //

        $request->validate([            
            'transaction_date',                       
            'particular'=>'required', 
            'debit',  
            'credit',
            'image_name' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
            
        
                
        ]);
        // $client_id=$request->get('client_id');  
        $client=Client::find($client_id); 
        $balance=(float)$request->get('credit')-(float)$request->get('debit'); 
         
        $client->balance=$client->balance+$balance;
        $client->save();

        if ($request->hasFile('image_name')) {
            $image = $request->file('image_name');
            $image_name = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/images');
            $image->move($destinationPath, $image_name);
            
        }


      
    
        $credit = new Credit([   
         'transaction_date' => $request->get('transaction_date'),                        
         'particular' => $request->get('particular'),
         'credit'=>$request->get('credit'),
         'debit'=>$request->get('debit'),
         'image_name'=>$image_name,
         'image_url'=>$destinationPath 

    
            
    ]);  
            
    $client->credits()->save($credit);


   
    return redirect()->route('viewCredit',$client_id)->with('success', 'Client saved!');
}
             

    
    
    
             
       
        //   return view('credits.viewCredit', compact('credits','client'));
         
    //       return view('credits.viewCredit')
    // ->with('credits', $client_id)
    // ->with('client', $client_id);

    

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id,$client_id)
    {
        //
         $credit = Credit::find($id);
         $client = Client::find($client_id);           
        return view('credits.edit', compact('credit','client'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id, $client_id)
    {
        //
         //
       $request->validate([            
           'transaction_date',                       
            'particular'=>'required', 
            'debit',  
            'credit',
            'image_name' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
                      
        ]);        
        $credit = Credit::find($id); 
        $client = Client::find($client_id);  
        $client->balance=$client->balance+($credit->debit-$credit->credit);
        $balance=(float)$request->get('credit')-(float)$request->get('debit'); 
         $client->balance=$client->balance+$balance;
       $client->save();
    //    Storage::delete(public_path('images/'.$credit->image_name));
       
    File::delete(public_path("/images/").$credit->image_name);  


        if ($request->hasFile('image_name')) {
            $image = $request->file('image_name');
            $image_name = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/images');
            $image->move($destinationPath, $image_name);
            
        }
        
        
       
         
             $credit->transaction_date=$request->get('transaction_date');                    
              $credit->particular=$request->get('particular');
              $credit->debit=$request->get('debit');
              $credit->credit=$request->get('credit');
              $credit->image_name=$image_name;
              $credit->image_url=$destinationPath;
              


                
               
        
        $credit->save();
       
       
            
        

        return redirect()->route('viewCredit',$client_id)->with('success', 'Client saved!');
    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,$client_id)
    {
        //
       
        $credit = Credit::find($id); 
        $client = Client::find($client_id);  
        $client->balance=$client->balance+($credit->debit-$credit->credit);
        $client->save();       
        $credit->delete();        
        return redirect()->route('viewCredit',$client_id)->with('success', 'Credit Deleted!');
 
   }

   
   public function viewCredit($client_id){
    //  client ko model bata field  taneko
    $client =Client::find($client_id); 
    $credits=$client->credits;
    return view('credits.viewCredit', compact('credits','client'));  
    // return view('credits.viewCredit')
    // ->with('credits', $client_id)
    // ->with('client', $client_id);

    // id lai liyera client bata data nikalne
}

public function addCredit($client_id){
    $client=Client::find($client_id);

    return view('credits.create',compact('client'));

    // return view('credits.create')
    //         ->with('credits', $client_id)
    //         ->with('client', $client_id);

} 







}
