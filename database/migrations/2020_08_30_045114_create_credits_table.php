<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCreditsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
         Schema::create('credits', function (Blueprint $table) {
            $table->id(); 
            $table->date('transaction_date')->useCurrent()->nullable();
            $table->string('particular');
            $table->string('credit')->nullable();
            $table->string('debit')->nullable();
            $table->unsignedBigInteger('client_id');
            $table->string('image_name')->nullable();
            $table->string('image_url')->nullable();
            $table->timestamp('updated_at')->useCurrent();
            $table->timestamp('created_at')->useCurrent();
            $table->foreign('client_id')->references('id')->on('clients')->onDelete('cascade');

        });
    }
    

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('credits');
    }
}
