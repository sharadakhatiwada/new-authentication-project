    @extends('layouts.app')

    @section('content')
  
    <div class="container">

       <div class="wrapper">
     <div class="row">
    
     
        <div class="col-2">
            <nav id="sidebar">


                <ul class="list-unstyled components">

                    <li>
                        <a href="#">Home</a>
                    </li>
                    <li class="active">
                        <a href="#clientSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Ledger</a>

                        <ul class="collapse list-unstyled" id="clientSubmenu">
                            <li>
                                <a href="{{ route('clients.index') }}">Client</a>

                            </li>
                             </ul>
                    </li>


                </ul>
            </nav>
        </div>

        <div class="col-10">
            @yield('homecontent')
        </div>
         </div>



    </div>


    </div>    

    @endsection
    




