

@extends('home')
@section('homecontent')

<div class="row">
    <div class="col-sm-12">    
        <h1 class="display-4">Clients</h1>   
        <a href="{{ route('clients.create') }}" class="btn btn-primary">Add</a>  
        <table class="table table-striped">    
            <thead>        
            <tr>          
                <td>ID</td>          
                <td>Name</td>
                <td>Address</td>  
                <td>Phone Number</td>
                 <td>Balance</td>

                <td colspan = 2>Actions</td>        
            </tr>    
            </thead>    
            <tbody>        
                @foreach($clients as $client)        
                    <tr>            
                        <td>{{$client->id}}</td>            
                        <td>{{$client->name}}</td>
                        <td>{{$client->address}}</td>
                          <td>{{$client->phonenumber}}</td>
                        <td>{{$client->balance}}</td>


                        <td>                                      
                            <a href="{{ route('clients.edit',$client->id)}}" class="btn btn-primary">Edit Client</a>
                                                                  

                        </td> 
                        <td>
                        <a href="{{ route('viewCredit',$client->id)}}" class="btn btn-primary">View Credit</a> 
                        </td> 
                        <td>  
                        <div target="_blank" >
                        <a href="{{ route('clients.printAllCredit',$client->id) }}"  class="btn btn-primary" >Print</a> 
                        </div>
                        </td>
                        <td>
                          <form action="{{ route('clients.destroy',$client->id )}}" method="post">                  
                                @csrf                  
                                @method('DELETE')                  
                                <button class="btn btn-danger" type="submit">Delete</button>                
                            </form>    
                                      
                        </td> 
                        
          
                        
                    </tr>        
                @endforeach    
            </tbody>  
        </table>
    <div>
</div>@endsection

