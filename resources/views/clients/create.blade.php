@extends('home')
@section('homecontent')
<div class="row"> 
    <div class="col-sm-8 offset-sm-2">    
        <h3 class="display-4">Add Client</h3>  
        <div>    
            @if ($errors->any())      
            <div class="alert alert-danger">        
                <ul>            
                @foreach ($errors->all() as $error)              
                <li>{{ $error }}</li>            
                @endforeach        
                </ul>      
            </div>
            <br />    
            @endif      
            <form method="post" action="{{ route('clients.store') }}">          
            @csrf          
            <div class="form-group">                  
                <label for="name">Name:</label>              
                <input type="text" class="form-control" name="name"/>          
            </div> 
            <div class="form-group">                  
                <label for="address">Address:</label>              
                <input type="text" class="form-control" name="address"/>          
            </div>  
            <div class="form-group">                  
                <label for="phonenumber">Phone Number:</label>              
                <input type="text" class="form-control" name="phonenumber"/>          
            </div> 
            <!-- <div class="form-group">                  
                <label for="balance">Balance:</label>              
                <input type="text" class="form-control" name="balance"/>          
            </div>                          -->
            <button type="submit" class="btn btn-success">Save</button>
            <a href="{{ route('clients.index') }}" class="btn btn-primary">Cancel</a>  
                
            </form>  
        </div>
    </div>
</div>
@endsection