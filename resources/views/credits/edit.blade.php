@extends('home')
@section('homecontent')
<div class="row">    
    <div class="col-sm-8 offset-sm-2">        
        <h3 class="display-4">Update Credit Record</h3>        
        @if ($errors->any())        
        <div class="alert alert-danger">            
            <ul>                
            @foreach ($errors->all() as $error)                
            <li>{{ $error }}</li>                
            @endforeach            
            </ul>        
        </div>        
        <br />         
        @endif        
        <form enctype="multipart/form-data"  method="post" action="{{ route('credits.update', [$credit->id,$client->id]) }}"> 
                        
        @method('PUT')             
        @csrf  
        
            <div class="form-group"> 

        
                           
                <label for="date">Date:</label>
                <input type="date" class="date-picker" name="transaction_date" value={{ $credit->transaction_date}} /> 

            
            </div>            
         <div class="form-group">                
                <label for="particular">Particular:</label>                
                <input type="text" class="form-control" name="particular" value={{ $credit->particular }} />  
            </div>   
            <div class="form-group">                
                <label for="credit">Credit:</label>                
                <input type="text" class="form-control" name="credit" value={{ $credit->credit }} />  
            </div> 
            <div class="form-group">                
                <label for="debit">Debit:</label>                
                <input type="text" class="form-control" name="debit" value={{ $credit->debit }} />  
            </div> 
            <div class="form-group d-flex flex-column">                
                <label for="image_name">Image:</label>                
                <input type="file"  name="image_name" value={{ $credit->image_name }} />  
            </div>  
            
             <button type="submit" class="btn btn-success">Update</button>        
            <a href="{{ route('viewCredit',$client->id) }}" class="btn btn-primary">Cancel</a>                        
        </form>
    </div>
</div>
@endsection