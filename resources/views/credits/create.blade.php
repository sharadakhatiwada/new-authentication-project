@extends('home')
@section('homecontent')



<div class="row"> 
    <div class="col-sm-8 offset-sm-2">    
        <h3 class="display-4">Add Credit</h3>  
        <div>    
            @if ($errors->any())      
            <div class="alert alert-danger">        
                <ul>            
                @foreach ($errors->all() as $error)              
                <li>{{ $error }}</li>            
                @endforeach        
                </ul>      
            </div>
            <br />    
            @endif      
            <form enctype="multipart/form-data" method="post" action="{{ route('credits.store',$client->id) }}">
            
          
                  
            @csrf  
      








            <div class="form-group"> 
                                 
                <label for="date">Date:</label>              
                <input type="text" id="" class="form-control date-picker " name="transaction_date"/> 

 

             
            </div> 

        
                
            <div class="form-group">                  
                <label for="particular">Particular:</label>              
                <input type="text" class="form-control" name="particular"/>          
            </div>  
            <div class="form-group">                  
                <label for="credit">Credit:</label>              
                <input type="text" class="form-control" name="credit"/>          
            </div> 
            <div class="form-group">                  
                <label for="debit">Debit:</label>              
                <input type="text" class="form-control" name="debit"/>          
            </div>
            <div class="form-group d-flex flex-column">                  
                <label for="image_name">Image:</label>          
                <input type="file"   name="image_name"/>  
                       
            </div> 
           


            
            <!-- <a href="{{ route('credits.index') }}" method="POST" enctype="multipart/form-date"></a>  -->
            
           

            <button type="submit" class="btn btn-success">Save</button>
            <a href="{{ route('viewCredit',$client->id) }}" class="btn btn-primary">Cancel</a> 
               
            </form>  
        </div>
    </div>
</div>
@endsection 
