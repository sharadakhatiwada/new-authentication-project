

@extends('home')
@section('homecontent')




<div class="row">
    <div class="col-sm-12">    
        <h1 class="display-4">View Credit Page</h1> 

        <table class="table table-striped">    
            <thead> 
                   
            <tr>          
                <td>Client ID</td>          
                <td>Name</td>
                <td>Address</td>  
                <td>Phone Number</td>
                </tr>    
            </thead> 
            <tbody>
            <tr>
                        <td>{{$client->id}}</td>            
                        <td>{{$client->name}}</td>
                        <td>{{$client->address}}</td>
                        <td>{{$client->phonenumber}}</td>
                        
            </tr>
            </tbody>  
           
        </table>
        <a href="{{ route('clients.index') }}" class="btn btn-primary">Back</a>        
        <a href="{{ route('addCredit',$client->id) }}" class="btn btn-primary">Add Credit</a>  
         
        <table class="table table-striped">    
            <thead> 

            <tr>  
                <td>ID</td>        
                <td>Date</td>          
                <td>Particular</td>
                <td>Credit</td>  
                <td>Debit</td>
                <td>Image</td>

                


                <td colspan = 2>Actions</td>        
            </tr>    
            </thead>    
            <tbody> 
            

            <!-- forloop layera gareko revery row create garna controller bata access gareko credits as credit        -->
                
                
                @foreach($credits as $credit)        
                    <tr>            
                        <td>{{$credit->id}}</td>            
                        <td>{{$credit->transaction_date}}</td>
                        <td>{{$credit->particular}}</td>
                        <td>{{$credit->credit}}</td>
                        <td>{{$credit->debit}}</td>
                        <td>{{$credit->image_name}}<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#imageModal-{{$credit->id}}">
  View
</button>
</td>


                       
                          <td>                                      
                          
                          <a href="{{ route('credits.edit',[$credit->id,$client->id]) }}" class="btn btn-primary">Edit Credit</a>      
                                      
                      </td>
                                                             
                          <td>
                          <form action="{{ route('credits.destroy',[ $credit->id, $client->id])}}" method="post">                  
                                @csrf                  
                                @method('DELETE')                  
                                <button class="btn btn-danger" type="submit">Delete</button>                
                            </form>    
                                      
                        </td> 
                                  
     
            
                    </tr>  
                    <!-- Modal -->
<div class="modal fade" id="imageModal-{{$credit->id}}" tabindex="-1" role="dialog" aria-labelledby="imageModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Image</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body"> 
      <img src="{{ URL::to('/') }}/images/{{$credit->image_name}}" width="100%">  
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>      
                @endforeach    
            </tbody>  
        </table>
    <div>
</div>@endsection


