

@extends('home')
@section('homecontent')
<div class="row">
    <div class="col-sm-12">    
        <h1 class="display-4">Credit</h1>   
        <a href="{{ route('credits.create') }}" class="btn btn-primary">Add Credit</a>  
        <table class="table table-striped">    
            <thead>        
            <tr>          
                <td>Date</td>          
                <td>Particular</td>
                <td>Credit</td>  
                <td>Debit</td>
                <td>Image</td>
                


                <td colspan = 2>Actions</td>        
            </tr>    
            </thead>    
            <tbody>        
                @foreach($credits as $credit)        
                    <tr>            
                        <td>{{$client->client_id}}</td>            
                        <td>{{$credit->transaction_date}}</td>
                        <td>{{$credit->particular}}</td>
                        <td>{{$credit->credit}}</td>
                        <td>{{$credit->debit}}</td>
                        <td>{{$credit->image_name}}</td>


                       
                         <td>                                      
                            <a href="{{ route('credits.edit',$credit->id)}}" class="btn btn-primary">Edit Credit</a>  
                                      
                        </td>  
                                   
                        
                    </tr>        
                @endforeach    
            </tbody>  
        </table>
    <div>
</div>@endsection

